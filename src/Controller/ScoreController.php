<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/score')]
class ScoreController extends AbstractController
{
    #[Route('/', name: 'app_score_index')]
    public function index(): Response
    {
        return $this->render('score/index.html.twig', [
            'controller_name' => 'ScoreController',
        ]);
    }

    #[Route('/', name: 'app_score_classement')]
    public function score(): Response
    {
        // cherche chaque 10 sec score dans db
        $scores = $this->getDoctrine()
            ->getRepository(Score::class)
            ->findBy([], ['value' => 'DESC'], 10);

        return $this->render('score/classement.html.twig', [
            'scores' => $scores,
        ]);
    }
}
