document.addEventListener('DOMContentLoaded', function() {

  let block = document.querySelector(".block");
  let start = document.querySelector(".start");
  let score = document.querySelector(".score");
  let secondes = document.querySelector(".secondes");
  let classementScore = document.querySelector("#classementScore");

  start.onclick = function() {
    let scoreValue = 0;
    let secondesValue = 15;
    block.innerHTML= "";
  
    let interval = setInterval(function showTarget() {
      // creation enemy
      let target = document.createElement('img');
      target.id = "target"
      target.src= "js/enemy.svg";
        
      // positionement & limitation du enemy
      block.appendChild(target);
      target.style.top = Math.floor(Math.random() * (400 - target.offsetHeight)) + 'px';
      target.style.left = Math.floor(Math.random() * (600 - target.offsetWidth)) + 'px';
  
        
      // faire disparaitre l enemy chaque 2seconde
      setTimeout(function() {
        target.remove()
      }, 2000)
  
      // augmentation score
      target.onclick = function() {
          scoreValue += 1;
          target.style.display = "none";
          
      }
        
      // diminution du temps
      secondesValue -= 1;
  
      // Seconde et Score
      secondes.innerHTML = `Secondes: ${secondesValue}`;
      score.innerHTML = `<p id="scorevalue" value="${scoreValue}">Score: ${scoreValue}</p>`;
  
      // game over
      if(secondesValue == 0) {
        clearInterval(interval);
        block.innerHTML = `<h3 style="text-align:center;">Game Over <br> Votre score est: ${scoreValue}</h3>`;
        
        // Afficher le classement
        let req = new XMLHttpRequest();
        req.open("POST", "{{ path('home') }}");
        req.onreadystatechange = function() {
          if (req.readyState === XMLHttpRequest.DONE && req.status === 200) {
            let classement = JSON.parse(req.responseText);
            let html = "";
            for (let i = 0; i < classement.length; i++) {
              html += `<li>${classement[i].nom} - ${classement[i].score}</li>`;
            }
            classementScore.innerHTML = html;
          }
        };
        req.send();
      }
  
    }, 1000)
  };
  
});
